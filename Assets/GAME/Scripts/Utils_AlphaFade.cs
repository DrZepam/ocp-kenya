﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utils_AlphaFade : MonoBehaviour {

    private CanvasGroup group;

    void Start()
    {
        group = GetComponentInChildren<CanvasGroup>();
        group.alpha = 0;
    }

    // helper methods are done here

    void FadeGroup(float start, float end, float time)
    {
        Hashtable ht = iTween.Hash(
            "from", start,
            "to", end,
            "time", time,
            "onupdate", "TweenAlpha"
            );

        iTween.ValueTo(gameObject, ht);
    }

    public void ShowText(float fadeDelay, float readDelay)
    {
        StartCoroutine(TextRoutine(fadeDelay, readDelay));
    }

    /// <summary>
    ///  Fade in and out the text object according to fadeDelay value
    ///  and keeps it visible for readDelay
    /// </summary>
    /// <param name="fadeDelay"></param>
    /// <param name="readDelay"></param>
    /// <returns></returns>
    IEnumerator TextRoutine(float fadeDelay, float readDelay)
    {
        FadeGroup(0, 1, fadeDelay);
        yield return new WaitForSeconds(fadeDelay + readDelay);
        FadeGroup(1, 0, fadeDelay);
    }


    // private methods are done here

    void TweenAlpha(float newValue)
    {
        group.alpha = newValue;
    }
}
