﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour {

    public MeshRenderer targetRenderer;

    public GameObject placeholderObj;
    public GameObject finalObj;
    public Material referenceMat;
    public GameObject objectCanvas;
    public Color initialColor;
    public Color targetColor;
    public Material outlineMat;
    private Color initcolor;


	// Use this for initialization

    public void Start()
    {
        // init the slots: show only placeholder in the first place
        placeholderObj.SetActive(true);
        finalObj.SetActive(false);
        GetComponent<BoxCollider>().enabled = false;
        referenceMat.color = initialColor;
        initcolor = initialColor;
    }

    public void SetSlot()
    {
        Debug.Log("Set Slot");
        placeholderObj.SetActive(false);
        finalObj.SetActive(true);
        GetComponent<BoxCollider>().enabled = false;
        GameManager.Instance.AddScore();
        this.enabled = false;
    }

    public void Invert()
    {
        placeholderObj.SetActive(true);
        finalObj.SetActive(false);
    }


    public void HighlightSlot()
    {
        referenceMat.color = initialColor;
        GetComponent<BoxCollider>().enabled = true;
        Hashtable ht = iTween.Hash(
                "from", initialColor,
                "to", targetColor,
                "time", 2.0f,
                "onupdate", "TweenMat",
                "oncomplete", "resetTween"
            );
            iTween.ValueTo(this.gameObject, ht);     
    }

    public void ErrorSlotEvent()
    {
        StartCoroutine(WrongEvent());
    }

    IEnumerator WrongEvent()
    {
        iTween.Stop(this.gameObject);
        referenceMat.color = Color.red;
        yield return new WaitForSeconds(1.0f);
        HighlightSlot();
    }
    public void ShowTextAnimation(float value)
    {
       
    }

    public void DelayedTextAnimation(float value)
    {
        
    }


    void TweenMat(Color newValue)
    {
        referenceMat.color = newValue;
    }

    void resetTween()
    {
        //iTween.Stop(this.gameObject);
        HighlightSlot();
    }

    // Material Switch method

    public void SwitchToOulines()
    {
        MeshRenderer[] rends = placeholderObj.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].material = outlineMat;
        }
    }
}
