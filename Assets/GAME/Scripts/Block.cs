﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    /*  Block Script
     *  handle block behaviours, including moving snapping and raycasting
     */

    #region Properties
    //public members
    public float xRayDistance;
    public float zRayDistance;
    public float movementSpeed;
    public bool isSnapped;
    //private members
    private Rigidbody _rigidbody;
    private RaycastHit _hitInfo;
    #endregion
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// debug drawStuff:
        Debug.DrawRay(transform.position, transform.right * xRayDistance, Color.red);
        Debug.DrawRay(transform.position, transform.right * -xRayDistance, Color.red);
        Debug.DrawRay(transform.position, transform.forward * -zRayDistance, Color.blue);
        Debug.DrawRay(transform.position, transform.forward * zRayDistance, Color.blue);
	}

    // EDITOR METHODS ARE DONE HERE

    void OnDrawGizmo()
    {

    }
}
