﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public struct TextData
{
    public GameObject target;
    public float fadeDelay;
    public float readTime;
    public UnityEvent targetEvent;
}

public class IntroManager : MonoBehaviour {
    public static IntroManager Instance;
    public Text textObject;
    public GameObject endgameContainer;
    public GameObject introContainer;
    public GameObject gameContainer;
    public GameObject canvasElement0;
    public GameObject canvasElement1;
    public GameObject rememberContainer;


    public TextData[] introTextData;
    public TextData[] slurryTextData;
    public TextData[] sulfuricTextData;
    public TextData[] phosphoricTextData;
    public TextData[] fertilizerTextData;
    public TextData[] solubleTextData;
    public TextData[] stage2TextData;
    public TextData[] outroTextData;

    public Font normal;
    public Font bold;
    public Font tekoFont;

    [TextArea(5,10)]
    public string[] introStrings;

    [TextArea(5, 10)]
    public string[] slurryEventStrings;

    [TextArea(5, 10)]
    public string[] sulfuricEventStrings;

    [TextArea(5, 10)]
    public string[] phosphoricEventStrings;

    [TextArea(5, 10)]
    public string[] fertilizerEventStrings;

    [TextArea(5, 10)]
    public string[] solubleEventStrings;

    [TextArea(5, 10)]
    public string[] stage2IntroStrings;

    [TextArea(5, 10)]
    public string[] outroStrings;
	// Use this for initialization
	void Awake () {
        Instance = this;
	}
	

    // helper methods are done here
    public void SetText(string value)
    {
        textObject.text = value;
    }

    public void DelayedText(string value, float delay, float midTextDelay = 5.0f)
    {

        textObject.text = value;
        StartCoroutine(TextFadeRoutine(delay, midTextDelay));
    }

    public void DelayedText(TextData value)
    {
        Utils_AlphaFade fade = value.target.GetComponent<Utils_AlphaFade>();
        value.target.SetActive(true);
        fade.ShowText(value.fadeDelay, value.readTime);
    }

    IEnumerator TextFadeRoutine(float delay, float midTextDelay = 5.0f)
    {
        yield return new WaitForSeconds(delay);
        Hashtable hashUp = iTween.Hash(
            "from", 0,
            "to", 1,
            "time", 1.0f,
            "onupdate", "AlphaTextUpdate"
            );
        iTween.ValueTo(gameObject, hashUp);
        yield return new WaitForSeconds(midTextDelay);
        Hashtable hashDown = iTween.Hash(
            "from", 1,
            "to", 0,
            "time", 1.0f,
            "onupdate", "AlphaTextUpdate"
            );
        iTween.ValueTo(gameObject, hashDown);
        yield return new WaitForSeconds(1.0f);
        textObject.text = string.Empty;
    }

    // privates 
    void AlphaTextUpdate(float newValue)
    {
        Color col = new Color(1, 1, 1, newValue);
        textObject.color = col;
    }

    public void IntroEvent()
    {
        StartCoroutine(IntroTextEvent(introTextData));
        // should change to music 1
        MusicManager.Instance.ChangeMusic(1);
    }

    public void SlurryTextEvent()
    {
        StartCoroutine(IntroTextEvent(slurryTextData, 0, 7.0f));
    }
    public void OutroEvent()
    {
        string userName = PlayerPrefs.GetString("Name", "user");
        outroStrings[0] = "Congratulations " + userName;
        StartCoroutine(ShowEndGameContainer());
    }
    public void SulfuricTextEvent()
    {
        StartCoroutine(IntroTextEvent(sulfuricTextData, 0, 7.0f));
    }
    public void PhosphoricTextEvent()
    {
        StartCoroutine(IntroTextEvent(phosphoricTextData,0, 7.0f));
    }
    public void FertilizerTextEvent()
    {
        StartCoroutine(IntroTextEvent(fertilizerTextData, 0, 7.0f));
    }
    public void SolubleTextEvent()
    {
        StartCoroutine(IntroTextEvent(solubleTextData,0,5.0f));
    }

    public void Stage2Event()
    {
        StopAllCoroutines();
        StartCoroutine(IntroLevel2Event(stage2TextData));
    }

    IEnumerator IntroTextEvent(string[] strings, float delay = 0, float midTextDelay = 5.0f)
    {
        yield return new WaitForSeconds(delay);
        DetectionComponent.Instance.Disable();
        for (int i = 0; i < strings.Length; i++)
        {
            DelayedText(strings[i], 0.5f, midTextDelay);
            yield return new WaitForSeconds(9.0f);
        }
        DetectionComponent.Instance.Enable();
        
    }

    IEnumerator IntroTextEvent(TextData[] strings, float delay = 0, float midTextDelay = 5.0f)
    {
        yield return new WaitForSeconds(delay);
        DetectionComponent.Instance.Disable();
        for (int i = 0; i < strings.Length; i++)
        {
            DelayedText(strings[i]);
            if (strings[i].targetEvent != null)
            {
                strings[i].targetEvent.Invoke();
            }
            float totalTime = (strings[i].fadeDelay * 2) + strings[i].readTime + 0.5f;
            yield return new WaitForSeconds(totalTime);
        }
        DetectionComponent.Instance.Enable();

    }

    IEnumerator IntroLevel2Event(string[] strings)
    {
        DetectionComponent.Instance.Disable();
        MusicManager.Instance.ChangeMusic(2);
        rememberContainer.SetActive(true);
        yield return new WaitForSeconds(12.0f);
        rememberContainer.SetActive(false);
        for (int i = 0; i < strings.Length; i++)
        {
            DelayedText(strings[i], 0.5f);
            yield return new WaitForSeconds(9.5f);
        }
        DetectionComponent.Instance.Enable();
    }

    IEnumerator IntroLevel2Event(TextData[] strings)
    {
        DetectionComponent.Instance.Disable();
        MusicManager.Instance.ChangeMusic(2);
        rememberContainer.SetActive(true);
        yield return new WaitForSeconds(12.0f);
        rememberContainer.SetActive(false);
        for (int i = 0; i < strings.Length; i++)
        {
            DelayedText(strings[i]);
            float totalTime = (strings[i].fadeDelay * 2) + strings[i].readTime + 0.5f;
            yield return new WaitForSeconds(totalTime);
        }
        ShootingGameManager.Instance.StartStage2();
        DetectionComponent.Instance.Enable();
    }


    // Intro Stuff:
    public void IntroRoutine()
    {
        StartCoroutine(IntroEventRoutine());
    }

    IEnumerator IntroEventRoutine()
    {
        introContainer.SetActive(false);
        canvasElement0.SetActive(true);
            IntroEvent();
        yield return new WaitForSeconds(28.0f);
        gameContainer.SetActive(true);
    }

    // End Screen Stuff:

    public IEnumerator ShowEndGameContainer()
    {
        DetectionComponent.Instance.Disable();

        for (int i = 0; i < outroTextData.Length; i++)
        {
            DelayedText(outroTextData[i]);
            float totalTime = (outroTextData[i].fadeDelay * 2) + outroTextData[i].readTime + 0.5f;
            yield return new WaitForSeconds(totalTime);
        }

        DetectionComponent.Instance.Enable();
        Text scoreTxt = endgameContainer.GetComponentInChildren<Text>();
        scoreTxt.text = "Your Score: " + (5000 - GameManager.Instance.GetScore()).ToString();
        endgameContainer.SetActive(true);

        // Score shit
        GameManager.Instance.SendScore();
    }

}
