﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJson;
using BestHTTP;
using UnityEngine.Networking;

[System.Serializable]
public class UserData
{
    public string user;
    public int score;
}

public class Test_API : MonoBehaviour {

    private UserData testUser = new UserData();
	// Use this for initialization
	void Start () {
        testUser.user = "UnityAPI";
        testUser.score = 100;
	}


    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            SendScore("Omar", 1337);
        }
    }
    // GET DATA

    IEnumerator WaitForWWW(WWW www)
    {
        yield return www;


        string txt = "";
        if (string.IsNullOrEmpty(www.error))
            txt = www.text;  //text of success
        else
            txt = www.error;  //error

        Debug.Log(txt);
    }
    void TaskOnClick()
    {
        StartCoroutine(PostRequest("http://188.166.171.249/scoreboard/?format=json"));
    }

    IEnumerator PostRequest(string url)
    {
        string bodyJsonString = JsonUtility.ToJson(testUser);
        Debug.Log("Data is: \n" + bodyJsonString);
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Accept", "application/json");
        yield return request.SendWebRequest();

        Debug.Log("Response: " + request.downloadHandler.text);
    }

    void BestPostRequest(UserData user)
    {
        string data = JsonUtility.ToJson(user);
        HTTPRequest request = new HTTPRequest(new System.Uri("http://188.166.171.249/scoreboard/?format=api"), HTTPMethods.Post, onRequestFinished);
        request.SetHeader("Content-Type", "application/json");
        request.RawData = System.Text.Encoding.UTF8.GetBytes(data);
        request.Send();
       
    }

    public void SendScore(string userName, int score)
    {
        UserData user = new UserData();
        user.user = userName;
        user.score = score;
        BestPostRequest(user);
    }

    void onRequestFinished(HTTPRequest req, HTTPResponse resp)
    {
        Debug.Log("Request Finished! Text received: " + resp.DataAsText);
    }
}
