﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DetectionComponent : MonoBehaviour {

    // Inspector properties
    public static DetectionComponent Instance;
    public Transform handAnchor;
    public GameObject lineObject;
    public Text debugText;
    private LineRenderer line;
    private RaycastHit hitInfo;
    private InteractiveObject lastObject;
    private bool canShoot;
    public GameObject bulletObj;
    public float shootValue = 0.3f;
    private float shootCounter;

    public float playTime { get; private set; }
    void Awake()
    {
        Instance = this;
        playTime = 0;
        shootCounter = shootValue;
    }
	// Use this for initialization
	void Start () {
        canShoot = true;
        if (lineObject != null)
        {
            GameObject lo = (GameObject)Instantiate(lineObject);
            line = lo.GetComponent<LineRenderer>();
        }	
	}
	
	// Update is called once per frame
	void Update () {

        shootCounter -= Time.deltaTime;
        if (!canShoot)
        {
            if (line != null)
            {
                line.SetPosition(0, handAnchor.position);
                line.SetPosition(1, handAnchor.position);
            }
            return;
        }
        // pointer laser stuff
        if (line != null)
        {
            line.SetPosition(0, handAnchor.position);
            line.SetPosition(1, handAnchor.position + handAnchor.forward * 100);
            // only add playtime if we have control
            playTime += Time.deltaTime;
        }

        // detection component part
        if (Physics.Raycast(handAnchor.position, handAnchor.forward, out hitInfo, Mathf.Infinity))
        {
            if (!ShootingGameManager.Instance.enabled)
            {
                InteractiveObject obj = hitInfo.collider.GetComponent<InteractiveObject>();
                if (obj != null)
                {
                    // laser stuff here
                    line.SetPosition(1, hitInfo.point);
                    lastObject = obj;
                    obj.OnInteractStart();
                    if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKey(KeyCode.Space))
                    {
                        obj.OnInteractionUpdate();
                        // drag feature
                        Vector3 pos = new Vector3(hitInfo.point.x, hitInfo.point.y, obj.transform.position.z);
                        obj.transform.localPosition = pos;

                    }
                    else
                    {
                        obj.OnInteractEnd();
                    }
                }
                else
                {
                    if (lastObject != null)
                    {
                        lastObject.OnPointExit();
                        lastObject.OnInteractEnd();
                        lastObject = null;
                    }
                    lastObject = null;
                }
            }

            UI_Button butt = hitInfo.collider.GetComponent<UI_Button>();
            if (butt != null)
            {
                if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
                {
                    butt.OnClick();
                }
            }
        }
        else
        {
            if (lastObject != null)
            {
                lastObject.OnPointExit();
                lastObject.OnInteractEnd();
                lastObject = null;
            }
        }

        if (ShootingGameManager.Instance.enabled)
        {
            if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
            {
                if (shootCounter <= 0)
                {
                    GameObject bullet = (GameObject)Instantiate(bulletObj, handAnchor.position, Quaternion.identity);
                    bullet.GetComponent<BulletObj>().Shoot(handAnchor.forward);
                    shootCounter = shootValue;
                }
            }
        }
	}

    public void Disable()
    {
        canShoot = false;
    }

    public void Enable()
    {
        canShoot = true;
    }

    // scoring crap are done here:

    public void ScoreAdd(float value)
    {
        playTime += value;
    }
}
