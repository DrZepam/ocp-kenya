﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMap : MonoBehaviour {

    /*  This script init the game map with an alpha material in its init
     */

    public static GameMap Instance;

    public Material alphaMat;
    private List<MeshRenderer> renderers;
    private List<Material> materials;

    void Awake()
    {
        Instance = this;
    }
    
    // Use this for initialization
	void Start () {
        InitMap();
	}
	
	// helper methods are done here

    void InitMap()
    {
        renderers = new List<MeshRenderer>();
        materials = new List<Material>();
        MeshRenderer[] rends = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer r in rends)
        {
            renderers.Add(r);
            materials.Add(r.material);
            r.material = alphaMat;

        }
    }

    // gameplay methods

    public void SetSlot(MeshRenderer target)
    {
        // Swap Material on loop, to be changed later
        for (int i = 0; i < renderers.Count; i++)
        {
            if (renderers[i] == target)
            {
                renderers[i].material = materials[i];
            }
        }
    }
}
