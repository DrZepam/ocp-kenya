﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilsLine : MonoBehaviour {

    public LineRenderer lineRend;
    public Transform[] linePos;
	// Use this for initialization
	void Start () {
        InitLine();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.F))
        {
            TweenLine(linePos[0].position, linePos[1].position);
        }
	}

    // helper methods are done here

    void InitLine()
    {
        lineRend.SetPosition(0, linePos[0].position);
        lineRend.SetPosition(1, linePos[0].position);
    }


    public void TweenLine(Vector3 from, Vector3 targetPos)
    {
        Hashtable ht = iTween.Hash(
            "from", from,
            "to", targetPos,
            "time", 1.0f,
            "onupdate", "MoveLine"
            );
        iTween.ValueTo(this.gameObject, ht);
    }

    void MoveLine(Vector3 newValue)
    {
        lineRend.SetPosition(1, newValue);
    }
}
