﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilsShip : MonoBehaviour {

    public Vector3 dir;
    public float moveSpeed;

    public bool isEnabled;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isEnabled)
        {
            transform.Translate(dir * moveSpeed * Time.deltaTime, Space.Self);
        }
	}
}
