﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public struct FPiece
{
    public Material targetMaterial;
}

public class InteractiveObject : MonoBehaviour
{

    #region Properties
    // inspector properties
    public MeshRenderer[] outline;
    public Slot targetSlot;
    public GameObject snapParticle;
    // Temporaty code
    private bool isDraggable;
    private Transform raycaster;
    private Vector3 initPosition;
    public Canvas textCanvas;
    public Utils_Rotate rotator;
    public UnityEvent onComplete;

    public AudioClip clickSound;
    public AudioClip errorSound;
    public AudioClip trueSound;
    private bool _onIdle;
    private bool _onPoint;
    private bool _onDrag;
    private bool _onSnap;
    #endregion

    #region Framework Methods
    // Use this for initialization
	void Start () {
        initPosition = transform.position;
        textCanvas.gameObject.SetActive(false);
        isDraggable = false;
        HideOutlines();
	}

    private void Update()
    {

    }
    #endregion

    #region Methods

    /// <summary>
    /// 
    /// </summary>
    public void OnInteractStart()
    {
        if (!isDraggable)
        {
            textCanvas.gameObject.SetActive(true);
            textCanvas.gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.01f, 0.01f, 0.01f);
            iTween.ScaleTo(textCanvas.gameObject, new Vector3(0.15f, 0.15f, 0.15f), 0.4f);
            
            isDraggable = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>

    public void OnInteractionUpdate()
    {
        if (isDraggable)
        {
            textCanvas.gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.01f, 0.01f, 0.01f);
            textCanvas.gameObject.SetActive(false);
            GameManager.Instance.PlayClip(clickSound);
            isDraggable = false;
            OnDrag();
        }
    }
    public void OnInteractEnd()
    {
        transform.position = initPosition;
        OnIdle();
        isDraggable = false;
    }
 

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Trigger Entered");
        Slot target = col.gameObject.GetComponent<Slot>();
        if (target != null && targetSlot == target)
        {
            target.SetSlot();
            OnSnap(col.transform);
            GameManager.Instance.PlayClip(trueSound);
            if (onComplete != null)
            {
                onComplete.Invoke();
            }
            Destroy(gameObject);
        }
        else if (target != null && targetSlot != target)
        {
            DetectionComponent.Instance.ScoreAdd(-10.0f);
            target.ErrorSlotEvent();
            GameManager.Instance.PlayClip(errorSound);
            OnInteractEnd();
        }
    }


    // Events are done here:
    public void OnIdle()
    {
        _onDrag = false;
        _onPoint = false;
        _onSnap = false;
        _onIdle = true;
        if (!rotator.enabled)
        {
            rotator.enabled = true;
        }
    }
    public void OnPointEnter()
    {

    }
    public void OnPointExit()
    {
        if (!textCanvas.gameObject.activeInHierarchy) {
            return;
        }
        textCanvas.gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.01f, 0.01f, 0.01f);
        textCanvas.gameObject.SetActive(false);
    }

    public void OnDrag()
    {
        if (!_onDrag)
        {
            rotator.enabled = false;
            rotator.gameObject.transform.localRotation = Quaternion.identity;
            ShowOutlines();
            _onDrag = true;
        }
    }

    public void OnSnap(Transform target)
    {
        GameObject particle = (GameObject)Instantiate(snapParticle, target.position, Quaternion.identity);
    }
    #endregion

    // Utils

    void ShowOutlines()
    {
        if (outline.Length > 0)
        {

        }
    }

    void HideOutlines()
    {

    }
}
