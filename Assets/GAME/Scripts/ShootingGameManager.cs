﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class ShootingGameManager : MonoBehaviour {

    public static ShootingGameManager Instance;

    [Header("Gameplay Stuff")]
    public GameObject moleculeObj;
    public GameObject[] goodSphere;
    public GameObject badSphere;
    public int moleculeNbr;

    [Header("Slot and placement")]
    public GameObject stage2Container;
    public GameObject[] slotObjects;
    public Text[] textSlots;
    private MoleculeObject firstTarget;
    private MoleculeObject secondTarget;
    public WaypointSystem.PathManager[] targetPaths;
    public WaypointSystem.PathManager firstPath;
    public WaypointSystem.PathManager secondPath; 
    [Header("Spawning attributes")]
    public Transform squarePos;  //world position of your square
    public Vector3 squareSize; //x = width, y = height, z = length of box
    public float squareBorder; //how far away from the edges you want the spawn to stay
    Vector3 realSquareSize;
    public UnityEvent finishEvent;
    private bool _hasfinished;
    private List<GameObject> moleculesList;
    public AudioClip errorSound;
    public AudioClip trueSound;
    public AudioClip bellSound;
    public AudioClip[] BGM;
    public int _phase { get; private set; }
    // End game particles
    public GameObject rewardParticle;


    void Awake()
    {
        Instance = this;
    }
	// Use this for initialization
	void Start () {
        Init();
	}
	

    // helper methods are done here

    void Init()
    {
        moleculesList = new List<GameObject>();
        realSquareSize = squareSize - (Vector3.one * squareBorder);
        IntroManager.Instance.Stage2Event();
        //StartStage2();
    }

    public void StartStage2()
    {
        StartCoroutine(SpawnMolecules());
    }
    IEnumerator SpawnMolecules()
    {
        for (int i = 0; i < moleculeNbr; i++)
        {
            if (i < 4)
            {
                // spawn only Two correct entities
                SpawnPrefabOnSquare(goodSphere[i],3.0f, true);

            }
            else
            {
                SpawnPrefabOnSquare(badSphere,2.0f);
            }
            yield return new WaitForSeconds(0.1f);
        }

        stage2Container.SetActive(true);
    }

    void SpawnPrefabOnSquare(GameObject spawn, float maxSpeed, bool isCorrect = false)
    {
        var spawnPos = squarePos.position + new Vector3(Random.Range(-realSquareSize.x, realSquareSize.x), Random.Range(-realSquareSize.y, realSquareSize.y), Random.Range(-realSquareSize.z, realSquareSize.z)) * 0.5f;
        GameObject spw = (GameObject)Instantiate(spawn, spawnPos, Quaternion.identity);

            moleculesList.Add(spw);

        spw.transform.SetParent(squarePos);
        WaypointSystem.splineMove spline = spw.GetComponent<WaypointSystem.splineMove>();
        WaypointSystem.PathManager path = targetPaths[Random.Range(0, targetPaths.Length)];
        spline.speed = Random.Range(1.0f, maxSpeed);
        spline.SetPath(path);
        spline.startPoint = Random.Range(0, path.GetWaypointCount());
        spline.StartMove();
    }

    public void SetSphere(MoleculeObject obj)
    {
        obj.transform.position = slotObjects[obj.phaseId].transform.position;
        _phase++;
        Compare();
    }

    void Compare()
    {
        if (_phase >= 4)
        {
            StartCoroutine(debugWin());
        }
    }

    private IEnumerator debugWin()
    {
        if (_hasfinished)
        {
            yield break;
        }
        _hasfinished = true;
        yield return new WaitForSeconds(2.0f);
        foreach (GameObject o in moleculesList)
        {
            if (o != null)
            {
                if (o.GetComponent<MoleculeObject>() != null)
                {
                    if (o.GetComponent<MoleculeObject>().isCorrect)
                    {
                        o.GetComponent<MoleculeObject>().WinPath();
                        GameManager.Instance.PlayClip(bellSound);
                        yield return new WaitForSeconds(0.8f);
                    }
                }
                Destroy(o);
            }
        }
        stage2Container.SetActive(false);
        yield return new WaitForSeconds(1.0f);

        GameManager.Instance.PlayClip(trueSound);
        EndGame();
    }

    private IEnumerator PhaseTwoRoutine(){
        GameManager.Instance.PlayClip(trueSound);
        yield return new WaitForSeconds(2.0f);
        firstTarget.WinPath();
        secondTarget.WinPath();
        firstTarget = null;
        secondTarget = null;
        // change texts
        textSlots[0].enabled = false;
        textSlots[2].enabled = true;
        _phase++;
        yield return new WaitForSeconds(1.0f);


    }
    IEnumerator Reset()
    {
        GameManager.Instance.PlayClip(errorSound);
        yield return new WaitForSeconds(2.0f);
        firstTarget.Reset();
        secondTarget.Reset();
        firstTarget = null;
        secondTarget = null;
    }

    public void EndGame()
    {
            finishEvent.Invoke();
    }
}
