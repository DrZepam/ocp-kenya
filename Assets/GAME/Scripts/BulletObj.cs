﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletObj : MonoBehaviour {

    private Vector3 dir;
    private bool _isMoving;
    private float _life;
    public float moveSpeed;
    public float life;

	// Use this for initialization
	void Start () {
        _life = life;
	}
	
	// Update is called once per frame
	void Update () {
        if (_isMoving)
        {
            _life -= Time.deltaTime;
            transform.Translate(dir * moveSpeed * Time.deltaTime);
            if (_life <= 0)
            {
                Destroy(gameObject);
            }
        }
	}

    // helper public methods

    public void Shoot(Vector3 direction)
    {
        dir = direction;
        _isMoving = true;
    }
}
