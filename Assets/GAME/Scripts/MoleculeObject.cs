﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleculeObject : MonoBehaviour {

    public bool isCorrect;
    public bool isCollidable;
    public string moleculeText;
    public int phaseId; // O or 1
    public GameObject particleBoom;
    public GameObject errorBoom;

    private WaypointSystem.splineMove splineController;
    public AudioClip clickSound;

	// Use this for initialization
	void Start () {
        splineController = GetComponent<WaypointSystem.splineMove>();

	}
	
	// Update is called once per frame
	void Update () {

	}


    // helper methods are done here

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            // check the particle stuff
            if (isCorrect)
            {
                Destroy(col.gameObject);
                SetSphere();
            }
            else
            {
                Destroy(col.gameObject);
                DetectionComponent.Instance.ScoreAdd(10);
                Instantiate(errorBoom, transform.position, Quaternion.identity);
                Destroy();
            }
        }
    }
    public void SetSphere()
    {
        GameManager.Instance.PlayClip(clickSound);
        splineController.Stop();
        GetComponent<SphereCollider>().enabled = false;
        ShootingGameManager.Instance.SetSphere(this);
    }

    public void Destroy()
    {
        // spawn Particle Effect and destroy

        Destroy(gameObject);

    }
    public void Reset()
    {
        GetComponent<SphereCollider>().enabled = true;
        splineController.StartMove();
    }

    public void WinPath()
    {
        if (isCorrect)
        {
            Instantiate(particleBoom, transform.position, Quaternion.identity);
        }
    }
}
