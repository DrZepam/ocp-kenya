﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    public List<GameObject> objectList;
    public Material mapMaterial;
    public GameObject mapObj;
    public Color mapColor;
    public Transform mapTransform;
    [Header("GPP")]
    public GameObject level1container;
    public GameObject level2Container;
    public GameObject lvl1Map;
    public GameObject lvl2Map;
    public Slot[] slotLevels;
    public GameObject mainCanvas;
    public int counter;
    public UtilsShip[] shipsObj;
    public GameObject rockObjs;
    public GameObject islandMesh;
    public AudioSource sfxSource;
    private bool hasFinished;
    public GameObject rewardParticle;
    //private Score shit
    public int score { get; private set; }
	// Use this for initialization
	void Awake () {
        Instance = this;
        mapMaterial.color = mapColor;
        hasFinished = false;
	}

    private void Start()
    {
        slotLevels[counter].HighlightSlot();
    }

    // Update is called once per frame
    void Update () {
        if (counter >= objectList.Count && !hasFinished)
        {
            hasFinished = true;
        }
	}

    public void FadeMap()
    {
        StartCoroutine(FadeMapRoutine());
    }
    public IEnumerator FadeMapRoutine()
    {
        yield return new WaitForSeconds(1.0f);
        foreach (GameObject g in objectList)
        {
            g.GetComponent<Slot>().placeholderObj.SetActive(true);
            g.GetComponent<Slot>().SwitchToOulines();
        }
        //rewardParticle.SetActive(true);
        yield return new WaitForSeconds(7.0f);
        //rewardParticle.SetActive(false);
        rockObjs.SetActive(false);
        //level2Container.SetActive(false);
        foreach (GameObject g in objectList)
        {
            g.GetComponent<Slot>().Invert();
        }
        Hashtable ht = iTween.Hash(
            "from", 0.5f,
            "to", 0,
            "time", 1.0f,
            "onupdate", "TweenSlider"
            );
        
        iTween.ValueTo(this.gameObject, ht);
        yield return new WaitForSeconds(1.0f);
        foreach (GameObject o in objectList)
        {
            o.GetComponent<Slot>().placeholderObj.SetActive(false);
        }
        mapObj.SetActive(false);
        mapMaterial.color = mapColor;
        yield return new WaitForSeconds(2.0f);
        // Let the rest start alone
        islandMesh.SetActive(false);
        GetComponent<ShootingGameManager>().enabled = true;
    }

    void TweenSlider(float newValue)
    {
        Color col = new Color(mapMaterial.color.r, mapMaterial.color.g, mapMaterial.color.b, newValue);
        mapMaterial.color = col;
    }

    public void AddScore()
    {
        counter++;
    }

    // Gameplay Events

    public void SlurryOnComplete()
    {
        StartCoroutine(MoveMap());
    }

    IEnumerator MoveMap()
    {
        // Move the first part to the left, and show the map
        yield return new WaitForSeconds(1.0f);
        Hashtable ht0 = iTween.Hash(
            "position", new Vector3(0, 0, 0),
            "time", 2.0f,
            "easetype", iTween.EaseType.easeInOutSine
            );
        iTween.MoveTo(mapTransform.gameObject, ht0);
        yield return new WaitForSeconds(2.0f);
        mapObj.SetActive(true);
        lvl2Map.SetActive(true);
        foreach (GameObject o in objectList)
        {
            if (o.GetComponent<Slot>().enabled)
            {
                o.GetComponent<Slot>().Invert();
            }
        }
        Hashtable ht = iTween.Hash(
            "from", 0,
            "to", 0.5f,
            "time", 1.0f,
            "onupdate", "TweenSlider"
            );
        iTween.ValueTo(this.gameObject, ht);
        yield return new WaitForSeconds(1.5f);
        // Show Text

        // Add Rocks
        rockObjs.SetActive(true);

        // wait a bit
        yield return new WaitForSeconds(5.0f);
        level2Container.SetActive(true);
        slotLevels[counter].HighlightSlot();
    }

    public void HighLightNext(Slot target)
    {
        target.HighlightSlot();
    }

    public void ShowTextAnimation(float value)
    {
        StartCoroutine(TextAnimationEvent(value));
    }
    IEnumerator TextAnimationEvent(float value)
    {
        yield return new WaitForSeconds(0.5f);
        mainCanvas.SetActive(true);
        yield return new WaitForSeconds(value);
        mainCanvas.SetActive(false);
    }

    public void StartShipAnim()
    {
        foreach (UtilsShip ship in shipsObj)
        {
            ship.isEnabled = true;
        }
    }

    public void TextEvent(string value)
    {
        IntroManager.Instance.DelayedText(value, 1.0f);
    }

    public void PlayClip(AudioClip clip)
    {
        sfxSource.Stop();
        sfxSource.clip = clip;
        sfxSource.Play();
    }

    // Scoring mechanism here:

    public int GetScore()
    {
        score = Mathf.RoundToInt(DetectionComponent.Instance.playTime);
        return score;
    }

    public void SendScore()
    {
        string name = PlayerPrefs.GetString("Name");
        int score = 5000 - GetScore();
        Test_API api = GetComponent<Test_API>();
        api.SendScore(name, score);
    }
}
