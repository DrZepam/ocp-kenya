﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class UI_Button : MonoBehaviour {

    public UnityEvent onClickEvent;


    //

    public void OnClick()
    {
        if (onClickEvent != null)
        {
            onClickEvent.Invoke();
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
