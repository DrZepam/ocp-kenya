﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    public static MusicManager Instance;
    public AudioClip[] BGM;
    private AudioSource source;

    void Awake()
    {
        Instance = this;
    }
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}

    // helper methods are done here

    public void ChangeMusic(int value){
        //source.Stop();
        //source.clip = BGM[value];
        //source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }
}
